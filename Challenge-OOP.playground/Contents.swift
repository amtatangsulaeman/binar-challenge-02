import UIKit

protocol carProtocol {
    var carName: String { set get }
    
    func go()
    func reverse()
}

class car: carProtocol {
    
    
    var carName: String
    init(){
        carName = ""
    }
    
    func go() {
        print("mobil \(carName) sedang maju")
    }
    
    func reverse() {
        print("mobil \(carName) sedang mundur")
    }
}

class taft: car {
    override func go() {
        carName = "Taft"
        print("mobil \(carName) sedang maju")
    }
    
    override func reverse() {
        carName = "Taft"
        print("mobil \(carName) sedang mundur")
    }
}

let Taft = taft()
Taft.reverse()
Taft.go()

